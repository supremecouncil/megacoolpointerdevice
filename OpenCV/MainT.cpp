#define blabla
#ifdef blabla
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include<math.h>
#include <iostream>

#include <stdio.h>
//
using namespace std;
using namespace cv;

/** Function Headers */
void detectAndCalculate( Mat frame, VideoCapture &cap);
void ShowFunctions_hc(const Mat &A, Mat &out);
void ShowFunctions_vc(const Mat &A, Mat &out);
void ShowFunctions_h_gs(Mat &A);
void ShowFunctions_h_gs2(Mat &A);
void cross(Mat &img,Point p, Scalar col, int size = 1);



class DataPointT{
private:
	vector<vector<double> > sumdataT; //two functions: horisontal(3 channals), vertical(3 channals)
	vector<vector<double> > weightsT, dispersionsT;
	int lastUpdateFrame;
	int experimentsN;
	int divStarts;
public:
	bool underExperiment;
	int x,y, thickness;
	Scalar color;
	friend double dataPointCMP_T(DataPoint A, DataPoint B, vector<vector<double> > sumsT );
	DataPointT(int sx, int sy, vector<vector<double> > sumsT):divStarts(5){
		underExperiment = false;
		lastUpdateFrame = -1;
		experimentsN = 1;
		sumdata = sums;
		thickness = 1;
		color = Scalar(0,100,0);
		x = sx;
		y = sy;

		dispersions.resize(sums.size());
		weights.resize(sums.size());
		for(int k = 0; k < 6; k++){
			double weightSum = 0;
			for(int i = 0; i < weights.size(); i++){
				dispersions[i].resize(6);
				weights[i].resize(6);
				weights[i][k] = 1;
				dispersions[i][k] = 0;
				weightSum += weights[i][k];
			}
			for(int i = 0; i < weights.size(); i++){
				weights[i][k] /= weightSum;
			}
		}
	}



	void update_sumsT(vector<vector<double> > new_sumsT, int frameN){
		if(frameN == lastUpdateFrame)
			return;
		for(int i = 0; i < sumdataT.size(); i++){
			for (int k = 0; k < 6; k++){
				sumdataT[k][i] = (sumdataT[k][i]*experimentsN + new_sumsT[k][i]) / (experimentsN +1);
			}
		}
		experimentsN++;

		if(experimentsN > divStarts){
			int divN = experimentsN - divStarts;
			for (int k = 0; k < 6; k++){
				double weightSum = 0;
				double weightlim = 500; //TODO: find correct formula
				for(int i = 0; i < sumdataT.size(); i++){
					//double deltaSq = pow( sumdata[i][k] - new_sums[i][k], 2); //TODO: consider abs instead;
					double deltaSq = abs( sumdata[i][k] - new_sums[i][k]); 
					double newDiv = (dispersions[i][k]*divN + deltaSq) / (divN +1);
					dispersions[i][k] = newDiv;
					double weight = 1/dispersions[i][k]; //TODO: find correct formula
					if(weight > weightlim){
						weight = weightlim;
					}
					weights[i][k] = weight;
					weightSum += weight;
				}

				bool coutMaxWeight = false; //debug info
				if(coutMaxWeight){
					double maxw = 0;
					for(int i  = 0; i < weights.size(); i++){
						if(maxw < weights[i][k]){
							maxw = weights[i][k];
						}
					}
					cout<<maxw<<endl;
				}

				for(int i = 0; i < sumdataT.size(); i++){
					weights[i][k] /= weightSum;
					assert(weights[i][k]<1);
				}
			}
		}
		//color = Scalar(0,(experimentsN*39)%130+20,(experimentsN*29) % 130+20);
	}

	void show(Mat &image){
		int xstart = x - weights.size()/2;
		int ystart = y + 50;

		bool showWeights = true; //debug info
		if(showWeights){ 
			for(int i = 1; i < weights.size(); i++){
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][0]),Point(xstart + i,ystart - weights.size()*5*weights[i][0]),Scalar(255,0,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][1]),Point(xstart + i,ystart - weights.size()*5*weights[i][1]),Scalar(0,255,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][2]),Point(xstart + i,ystart - weights.size()*5*weights[i][2]),Scalar(0,0,255));
			}
			for(int i = 1; i < weights.size(); i++){
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][3]),Point(xstart + i,ystart - weights.size()*5*weights[i][3]),Scalar(255,255,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][4]),Point(xstart + i,ystart - weights.size()*5*weights[i][4]),Scalar(0,255,255));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][5]),Point(xstart + i,ystart - weights.size()*5*weights[i][5]),Scalar(255,0,255));
			}
		}

		bool showSums = false; //debug info
		if(showSums){
			for(int i = 1; i < sumdataT.size(); i++){
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*0.5*sumdataT[0][i-1]),Point(xstart + i,ystart - sumdataT.size()*0.5*sumdataT[0][i]),Scalar(255,0,0));
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*0.5*sumdataT[1][i-1]),Point(xstart + i,ystart - sumdataT.size()*0.5*sumdataT[1][i]),Scalar(0,255,0));
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*0.5*sumdataT[2][i-1]),Point(xstart + i,ystart - sumdataT.size()*0.5*sumdataT[2][i]),Scalar(0,0,255));
			}
			for(int i = 1; i < sumdataT.size(); i++){
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*5*sumdataT[3][i-1]),Point(xstart + i,ystart - sumdataT.size()*5*sumdataT[3][i]),Scalar(255,255,0));
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*5*sumdataT[4][i-1]),Point(xstart + i,ystart - sumdataT.size()*5*sumdataT[4][i]),Scalar(0,255,255));
				line(image, Point(xstart + i-1,ystart - sumdataT.size()*5*sumdataT[5][i-1]),Point(xstart + i,ystart - sumdataT.size()*5*sumdataT[5][i]),Scalar(255,0,255));
			}
		}

		cross(image, Point(x,y),color,thickness);
		if(underExperiment){
			if(experimentsN > divStarts*2 + 10){
				ellipse( image, Point(x,y), Size( 10, 10), 0, 0, 360, Scalar( 0, 200, 0 ), 2, 8, 0 );
			}else{
				ellipse( image, Point(x,y), Size( 10, 10), 0, 0, 360, Scalar( 0, 80, 150 ), 2, 8, 0 );
			}
		}
	}
	void highlite(Mat &image){
		color += Scalar(0,80,0);
		thickness+=3;
		show(image);
		thickness-=3;
		color -= Scalar(0,80,0);
	}
};

double vectorDiff(vector<double> A, vector<double> B){
	int res = 0;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += abs(A[i] - B[i]);
	}
	return res;
}

double vectorDiffSq(vector<double> A, vector<double> B){
	int res = 0;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += pow(A[i] - B[i],2);
	}
}

double vectorDiffWeighted(vector<double> A, vector<double> B, vector<double> w){
	int res = 0;
	double w_sum;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += abs(A[i] - B[i])*w[i];
		w_sum += w[i];
	}
	return res/w_sum;
}

double vectorDiffSqWeighted(vector<double> A, vector<double> B, vector<double> w){
	int res = 0;
	double w_sum;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += pow(A[i] - B[i],2)*w[i];
		w_sum += w[i];
	}
	return res/w_sum;
}

/** Result is <0 if A is closer to cur, >0 if B is closer to cur */
double vectorSmartCMP(vector<double> A, vector<double> B, vector<double> cur, vector<double> AWeights, vector<double> BWeights ){
	double choice = 0;
	double w_sum = 0;
	int size = A.size();
	assert(size == B.size && size == cur.size);
	for(int i = 0; i < size; i++){
		for(int k = 0; k < 3; k++){
			double w = (B[i] - A[i])*AWeights[i]*BWeights[i]; //TODO: find correct formula
			double x = cur[i];
			double mid = (A[i] + B[i])/2.;
			choice += (x - mid)*w;
			w_sum += abs(w);
		}
	}
	choice /= w_sum;
	return choice;
}

double dataPointCMPT(DataPoint A, DataPoint B, vector<vector<double> > sumsT ){
	double choice = 0, choice_h = 0, choice_v = 0;
	double w_h_sum = 0, w_v_sum = 0;

	for(int i = 0; i < sums.size(); i++){
		for(int k = 0; k < 3; k++){
			double w = (B.sumdata[i][k] - A.sumdata[i][k])*A.weights[i][k]*B.weights[i][k]; //TODO: find correct formula
			double x = sums[i][k];
			double mid = (A.sumdata[i][k] + B.sumdata[i][k])/2.;
			//double choicel = (xl - midl)*(wl>0?1:-1);
			choice_h += (x - mid)*w;
			w_h_sum += abs(w);
		}
		for(int k = 3; k < 6; k++){
			double w = (B.sumdata[i][k] - A.sumdata[i][k])*A.weights[i][k]*B.weights[i][k];
			double x = sums[i][k];
			double mid = (A.sumdata[i][k] + B.sumdata[i][k])/2.;
			//double choicel = (xl - midl)*(wl>0?1:-1);
			choice_v += (x - mid)*w;
			w_v_sum += abs(w);
		}
	}
	choice_h /= w_h_sum;
	choice_v /= w_v_sum;

	//TODO:remove, it is debug only
	//double choice_h_n, choice_v_n;
	//choice_h_n = vectorSmartCMP(A.sumdata);

	double w_h, w_v;
	w_h = abs(A.x - B.x) + 0.1 * abs(A.y - B.y);
	w_v = abs(A.y - B.y) + 0.1 * abs(A.x - B.y);// TODO: find good constants
	//w_h = w_v;
	return choice_h * w_h + choice_v * w_v;
	//return choice/Sums.sum_hl.size();
	//return choice/w_sum;
}

vector<DataPoint> points;

vector<vector<double> > concatSumsLRT(vector<vector<double> > sumLT, vector<vector<double> >sumRT){
	vector<vector<double> > resT = sumL;
	res.insert( res.end(), sumR.begin(), sumR.end() );
	return res;
}

vector<vector<double> > mergeHorizVertT(vector<vector<double> > sum_hT, vector<vector<double> >sum_vT){ //3d + 3d -> 6d
	vector<vector<double> > resT = sum_h;
	for(int i = 0; i < res.size(); i++){
		res[i].push_back(sum_v[i][0]);
		res[i].push_back(sum_v[i][1]);
		res[i].push_back(sum_v[i][2]);
	}
	return res;
}

struct SUMS{
	vector<vector<double> > sum_hlT, sum_hrt, sum_vlT, sum_vrT;
};
vector<vector<double> > SUMS2vecT(SUMS sT){
	vector<vector<double> > resT, sumst_hT, sumst_vT;
	sumst_h = concatSumsLR(s.sum_hl, s.sum_hr);
	sumst_v = concatSumsLR(s.sum_vl, s.sum_vr);
	res = mergeHorizVert(sumst_h,sumst_v);
	return res;
}

/** Global variables */
SUMS Sums, TotalR, TotalL;
std::vector<vector<double> > SumHT(const Mat &A);
std::vector<vector<double> > SumVT(const Mat &A);
String face_cascade_name = "haarcascade_mcs_eyepair_big.xml";
CascadeClassifier face_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);
int slider1,slider2,slider3,slider4,slider5;
int frameN;
Rect prevFaces;
bool prevExists = false;
Mat prevFrame;
const int MAXX = 1920, MAXY = 1080;
Mat image(MAXY,MAXX,CV_8UC3);

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if  ( event == EVENT_LBUTTONDOWN ){
		DataPoint tmp(x,y,SUMS2vec(Sums));
		tmp.underExperiment = true;
		points.push_back(tmp);
	}
	else if  ( event == EVENT_RBUTTONDOWN ){//update dispersions
		//1. find closest cross to the cursor
		int min_distance = 0x7fffffff;
		int min_i;
		for(int i = 0; i < points.size(); i++){
			DataPoint &pt = points[i];
			int distanceSq = pow((x - pt.x),2) + pow((y - pt.y),2);
			if(min_distance > distanceSq){
				min_i = i;
				min_distance = distanceSq;
			}
		}
		//update
		points[min_i].underExperiment = true;
	}
	else if  ( event == EVENT_RBUTTONUP || event == EVENT_LBUTTONUP ){//finish experiments
		for(int i = 0; i < points.size(); i++){
			points[i].underExperiment = false;
		}
	}
	else if  ( event == EVENT_MBUTTONDOWN )
	{
		// cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
	}
	else if ( event == EVENT_MOUSEMOVE )
	{
		// cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;
	}    
}


double getDirectionH(){ //deprecated
	double choice = 0;
	double w_sum = 0;
	for(int i = 0; i < Sums.sum_hl.size(); i++){
		double d = 0;
		double midl[3], midr[3], wl[3], wr[3];
		for(int k = 0; k < 3; k++){
			double wl = TotalR.sum_hl[i][k] - TotalL.sum_hl[i][k];
			double xl = Sums.sum_hl[i][k];
			double midl = (TotalL.sum_hl[i][k] + TotalR.sum_hl[i][k])/2.;
			//double choicel = (xl - midl)*(wl>0?1:-1);
			double choicel = (xl - midl)*wl;
			w_sum += abs(wl);

			double wr = TotalR.sum_hr[i][k] - TotalL.sum_hr[i][k];
			double xr = Sums.sum_hr[i][k];
			double midr = (TotalL.sum_hr[i][k] + TotalR.sum_hr[i][k])/2.;
			//double choicer = (xr - midr)*(wr>0?1:-1);
			double choicer = (xr - midr)*wr;
			choice += choicel + choicer;
			w_sum += abs(wr);
		}
	}
	//return choice/Sums.sum_hl.size();
	return choice/w_sum*3;
}

void cross(Mat &img, Point p, Scalar col, int size){
	line(img, p - Point(10,10), p+Point(10,10), col,size);
	line(img, p - Point(10,-10), p+Point(10,-10), col,size);
}

void showDir(){
	image.setTo(Scalar(255,255,255)); //white background leads to higher quality
	//image.setTo(Scalar(0,0,0)); //black background more comfortable for staring
	for(int i = 0; i < points.size(); i++){
		points[i].show(image);
	}
	if(points.size() > 0){
		int i_max;
		DataPoint v_max = points[0];
		for(int i = 0; i < points.size(); i++){
			if(dataPointCMP(v_max, points[i], SUMS2vec(Sums)) >0 ){
				v_max = points[i];
				i_max = i;
			}
		}
		v_max.highlite(image);
	}
	imshow("dir", image);
}



/** @function main */
int main( int argc, const char** argv ){
	VideoCapture cap(0); // open the default camera
	if(!cap.isOpened())  // check if we succeeded
		return -1;

	//create windows
	namedWindow("w1",1);
	createTrackbar("l","w1",&slider1,1000);
	createTrackbar("2","w1",&slider2,1000);
	createTrackbar("3","w1",&slider3,1000);
	createTrackbar("4","w1",&slider4,1000);
	createTrackbar("5","w1",&slider5,1000);

	//-- 1. Load the cascades
	if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

	//-- 2. Read the video stream
	Mat frame;
	frameN = 0;
	while( true ){
		//frame = cvQueryFrame( capture );
		cap>>frame;
		frameN++;
		//imshow("Cam",frame);
		//-- 3. Apply the classifier to the frame

		if( !frame.empty() ){ 
			detectAndCalculate( frame , cap); //calculate global var Sums

			for(auto &pt : points){ // update points
				if(pt.underExperiment){
					pt.update_sums(SUMS2vec(Sums),frameN);
					break;
				}
			}
		}
		else{ 
			printf(" --(!) No captured frame -- Break!"); 
			break; 
		}

		int c = waitKey(10);
		if( (char)c == 27 ) { break; }
		
		setMouseCallback("dir", CallBackFunc, NULL);
		showDir();
	}
	return 0;
}

//Rect getEyesPosFromCam(VideoCapture &cap){
//	Mat frame;
//	cap>>frame;
//	std::vector<Rect> faces;	
//	Mat frame_gray;
//	cvtColor( frame, frame_gray, CV_BGR2GRAY );
//	equalizeHist( frame_gray, frame_gray );
//	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(15, 15) );
//	return faces[0];
//}

/** @function detectAndDisplay */
void detectAndCalculate( Mat frame , VideoCapture &cap)
{
	//-- Detect eyes
	std::vector<Rect> faces;
	Mat frame_gray;
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );
	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

	if(faces.size()){
		size_t faceit = 0;
		int cutTop = 80,cutBottom = 80; //cut eyebrows
		const int funSize = 500; //size of each eye's image. //TODO: find good size
		int rHeight = funSize/2+cutTop+cutBottom, rWidth = funSize; //Image must be squre, to allow dataPointCMP() work.

		if(!prevExists){
			prevExists = true;
			prevFaces = faces[faceit];
			prevFrame = frame.clone(); 
		}

		// mixing frame with previous to reduce noise
		double frameMixAlpha = 0.5;
		Mat nextFrame = frame.clone();
		addWeighted( frame, frameMixAlpha, prevFrame, 1-frameMixAlpha, 0.0, nextFrame);

		prevFrame = frame.clone();
		frame = nextFrame.clone();

		//stabilization of eye detection
		double mult =2;
		faces[faceit].x = (faces[faceit].x + mult*prevFaces.x)/(1.0 + mult);
		faces[faceit].y = (faces[faceit].y + mult*prevFaces.y)/(1.0 + mult);
		faces[faceit].width = (faces[faceit].width + mult*prevFaces.width)/(1 + mult);
		faces[faceit].height = (faces[faceit].height + mult*prevFaces.height)/(1 + mult);
		prevFaces = faces[faceit];

		//Mat faceROI = frame_gray( faces[faceit] ); 
		Mat faceROIc = frame(faces[faceit]);
		Mat faceROIcr;
		Mat leftHalf, rightHalf;
		resize(faceROIc, faceROIcr,Size(rWidth, rHeight), 0, 0, CV_INTER_CUBIC);
		faceROIcr = faceROIcr(Rect(0, cutTop, rWidth, rHeight - cutTop - cutBottom));
		rHeight -= cutBottom + cutTop;
		leftHalf = faceROIcr(Rect(0,0,rWidth/2,rHeight));
		rightHalf = faceROIcr(Rect(rWidth/2,0,rWidth/2,rHeight));


		Sums.sum_hl = SumH(leftHalf); //pice of code, the function was called for
		Sums.sum_vl = SumV(leftHalf);
		Sums.sum_hr = SumH(rightHalf);
		Sums.sum_vr = SumV(rightHalf);

		bool showEyes = true;  // debug info
		if(showEyes){
			Mat leftRes = leftHalf.clone();
			Mat rightRes = rightHalf.clone();
			Mat leftRes_v = leftHalf.clone();
			Mat rightRes_v = rightHalf.clone();
			ShowFunctions_hc(leftHalf,leftRes);
			ShowFunctions_vc(rightHalf,leftRes_v);
			imshow( window_name+std::to_string(faceit), leftRes);
			imshow( window_name+std::to_string(faceit)+"v", leftRes_v);
			ShowFunctions_hc(rightHalf,rightRes);
			ShowFunctions_vc(rightHalf,rightRes_v);
			imshow( window_name+std::to_string(faceit)+"1", rightRes);
			imshow( window_name+std::to_string(faceit)+"1"+"v", rightRes_v);
		}

		// some experiments

		//cvtColor( faceROIcr, faceROIr_gs, CV_BGR2GRAY );
		//equalizeHist( faceROIr_gs, faceROIr_gs );
		//GaussianBlur(faceROIr_gs,faceROIcr_blur,Size(15,15),4,4);
		//faceROIr_gs_antilight = (faceROIr_gs*slider1 / faceROIcr_blur);
		//GaussianBlur(faceROIcr,faceROIcr_blur,Size(15,15),4,4);
		//faceROIcr_antilight = (faceROIcr*slider1 / faceROIcr_blur);

		//Mat edges = faceROIcr.clone(); 
		//GaussianBlur(edges, edges, Size(2*(slider2/100+1)+1,2*(slider2/100+1)+1), slider1/100., slider1/100.);
		//edges *= slider3/200.;
		//Canny(edges, edges, 0, 30, 3);
		//GaussianBlur(edges, edges, Size(2*(slider3/100+1)+1,2*(slider3/100+1)+1), slider4/100., slider4/100.);
		//imshow("edges", edges*slider5/100.);


		//Mat leftFlipped, rightFlipped;
		//flip(leftHalf,leftFlipped, 1);
		//flip(rightHalf,rightFlipped, 1);
		//imshow( window_name+std::to_string(faceit)+"2", leftFlipped);
		//imshow( window_name+std::to_string(faceit)+"3", rightFlipped);
		//ShowFunctions_hc(faceROIcr);
		//imshow( window_name+std::to_string(faceit), faceROIcr);
		//ShowFunctions_h_gs2(faceROIr_gs);
		//imshow( window_name+std::to_string(faceit)+"1", faceROIr_gs);
		//ShowFunctions_hc(faceROIcr_antilight);
		//imshow( window_name+std::to_string(faceit)+"2", faceROIcr_antilight);
		//ShowFunctions_h_gs(faceROIr_gs_antilight);
		//imshow( window_name+std::to_string(faceit)+"3", faceROIr_gs_antilight);
		//ShowFunctions_hc(faceROIcr_blur);
		//imshow( window_name+std::to_string(faceit)+"4", faceROIcr_blur);
	}
}

void ShowFunctions_h_gs(Mat &A){ //deprecated
	std::vector<double> hsumGS;
	hsumGS.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		hsumGS[i] = 1;
	}

	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			hsumGS[j]+= A.at<cv::Vec3b>(i,j)[0]/255.0+(slider2/500.);
		}
	}
	for(int j = 0; j < A.cols; j++){
		hsumGS[j] = (hsumGS[j] - slider3)*(slider4+1)/(slider5+1);
	}

	for(int i = 1; i < A.cols; i++){
		line(A, Point(i-1,hsumGS[i-1]), Point(i,hsumGS[i]),Scalar(255,0,0),1);
	}
}

void ShowFunctions_h_gs2(Mat &A){ //deprecated
	std::vector<double> hsumGS;
	hsumGS.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		hsumGS[i] = 1;
	}

	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			hsumGS[j]*= A.at<cv::Vec3b>(i,j)[0]/255.0+(slider2/500.);
		}
	}
	for(int j = 0; j < A.cols; j++){
		hsumGS[j] = (hsumGS[j] - slider3)*(slider4+1)/(slider5+1);
	}

	for(int i = 1; i < A.cols; i++){
		line(A, Point(i-1,hsumGS[i-1]), Point(i,hsumGS[i]),Scalar(255,0,0),1);
	}
}

std::vector<vector<double> > SumHT(const Mat &A){
	std::vector<vector<double> > resT;
	double E[3] = {0,0,0};
	res.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		res[i].resize(3);
		res[i][0] = 0;
		res[i][1] = 0;
		res[i][2] = 0;
	}
	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			for(int k = 0; k < 3; k++)
				res[j][k] += A.at<cv::Vec3b>(i,j)[k]/256.;
		}
	}
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++)
			E[k] += res[j][k]/A.cols;
	}
	double min[3] = {10e30,10e30,10e30}, max[3] = {-10e30,-10e30,-10e30}; 
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++){
			//res[j][k] -= E[k];
			if(min[k] > res[j][k])
				min[k] = res[j][k];
			if(max[k] < res[j][k])
				max[k] = res[j][k];
		}
	}
	double delta[3] = {max[0] - min[0], max[1] - min[1], max[2] - min[2]};
	double mid[3] = {(max[0] + min[0])/2, (max[1] + min[1])/2, (max[2] + min[2])/2};
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++){
			res[j][k] -= mid[k];
			res[j][k] /= delta[k];
		}
	}
	return res;
}

std::vector<vector<double> > SumVT(const Mat &A){
	Mat B;
	transpose(A,B);
	return SumH(B);
}

void ShowFunctions_hc(const Mat &A, Mat &out){
	int mid = A.rows/2;
	std::vector<std::vector<double> > sum;
	sum = SumH(A);
	for(int i = 1; i < A.cols; i++){
		for(int k = 0; k < 3; k++)
			line(out, Point(i-1,-sum[i-1][k]*A.rows + mid), Point(i,-sum[i][k]*A.rows + mid),Scalar(255*(k==0),255*(k==1),255*(k==2)),1);
	}
}


void ShowFunctions_vc(const Mat &B, Mat &out){
	Mat A,C;
	transpose(B,A);
	transpose(out,C);
	int mid = A.rows/2;
	std::vector<std::vector<double> > sum;
	sum = SumH(A);
	for(int i = 1; i < A.cols; i++){
		for(int k = 0; k < 3; k++)
			line(C, Point(i-1,sum[i-1][k]*A.rows + mid), Point(i,sum[i][k]*A.rows + mid),Scalar(255*(k==0),255*(k==1),255*(k==2)),1);
	}
	transpose(C,out);

}
#endif