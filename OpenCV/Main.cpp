#define blabla
#ifdef blabla
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <math.h>
#include <iostream>
#include <utility>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

using namespace std;
using namespace cv;

/** Function Headers */
void detectAndCalculate( Mat frame, VideoCapture &cap);
void ShowFunctions_hc(const Mat &A, Mat &out);
void ShowFunctions_vc(const Mat &A, Mat &out);
void ShowFunctions_h_gs(Mat &A);
void ShowFunctions_h_gs2(Mat &A);
void cross(Mat &img,Point p, Scalar col, int size = 1);

/** Config varibles */
bool debug_show_functuins = false;
bool debug_show_weights = false;
bool debug_show_eyes = false;
bool debug_show_frame = false;
bool calculate_interpolations = false;
bool debug_show_interpolations = false;
bool freezeUnderExperiment = true;
bool debug_time = true;

class DataPoint{
private:
	vector<vector<double> > sumdata; //two functions: horisontal(3 channals), vertical(3 channals)
	vector<vector<double> > weights, dispersions;
	int lastUpdateFrame;
	int experimentsN;
	int divStarts;
public:
	bool underExperiment;
	int x,y, thickness;
	Scalar color;
	friend double dataPointCMP(DataPoint A, DataPoint B, vector<vector<double> > sums );
	friend double dataPointCMP_h(DataPoint A, DataPoint B, vector<vector<double> > sums );
	friend double dataPointCMP_v(DataPoint A, DataPoint B, vector<vector<double> > sums );
	friend  double interpolationFunction(double x, double y, vector<DataPoint> points);
	DataPoint(int sx, int sy, vector<vector<double> > sums):divStarts(10){
		underExperiment = false;
		lastUpdateFrame = -1;
		experimentsN = 1;
		sumdata = sums;
		thickness = 1;
		color = Scalar(0,100,0);
		x = sx;
		y = sy;

		dispersions.resize(sums.size());
		weights.resize(sums.size());
		for(int k = 0; k < 6; k++){
			double weightSum = 0;
			for(int i = 0; i < weights.size(); i++){
				dispersions[i].resize(6);
				weights[i].resize(6);
				weights[i][k] = 1;
				dispersions[i][k] = 0;
				weightSum += weights[i][k];
			}
			for(int i = 0; i < weights.size(); i++){
				weights[i][k] /= weightSum;
			}
		}
	}

#ifdef ArqwerET_closeness // some not usefull functions
	double closeness(vector<vector<double> > other){
		double res = 0;
		for(int i = 0; i < sumdata.size(); i++)
			for(int k = 0; k < 6; k++)
				res -= abs(sumdata[i][k] - other[i][k])*weights[i][k];
		return res/weightSum;
	}

	double closeness_h(vector<vector<double> > other){
		double res = 0;
		for(int i = 0; i < sumdata.size(); i++)
			for(int k = 0; k < 3; k++)
				res -= abs(sumdata[i][k] - other[i][k])*weights[i][k];
		return res/weightSum;
	}

	double closeness_v(vector<vector<double> > other){
		double res = 0;
		for(int i = 0; i < sumdata.size(); i++)
			for(int k = 3; k < 6; k++)
				res -= abs(sumdata[i][k] - other[i][k])*weights[i][k];
		return res/weightSum;
	}
#endif

	void update_sums(vector<vector<double> > new_sums, int frameN){
		if(frameN == lastUpdateFrame)
			return;
		lastUpdateFrame = frameN;
		for(int i = 0; i < sumdata.size(); i++){
			for (int k = 0; k < 6; k++){
				sumdata[i][k] = (sumdata[i][k]*experimentsN + new_sums[i][k]) / (experimentsN +1);
			}
		}
		experimentsN++;

		if(experimentsN > divStarts){
			int divN = experimentsN - divStarts;
			for (int k = 0; k < 6; k++){
				double weightSum = 0;
				double weightlim = 500; //TODO: find correct formula
				for(int i = 0; i < sumdata.size(); i++){
					//double deltaSq = pow( sumdata[i][k] - new_sums[i][k], 2); //TODO: consider abs instead;
					double deltaSq = abs( sumdata[i][k] - new_sums[i][k]);
					double newDiv = (dispersions[i][k]*divN + deltaSq) / (divN +1);
					dispersions[i][k] = newDiv;
					double weight = 1/dispersions[i][k]; //TODO: find correct formula
					if(weight > weightlim){
						weight = weightlim;
					}
					weights[i][k] = weight;
					weightSum += weight;
				}

				bool coutMaxWeight = false; //debug info
				if(coutMaxWeight){
					double maxw = 0;
					for(int i  = 0; i < weights.size(); i++){
						if(maxw < weights[i][k]){
							maxw = weights[i][k];
						}
					}
					cout<<maxw<<endl;
				}

				for(int i = 0; i < sumdata.size(); i++){
					weights[i][k] /= weightSum;
					assert(weights[i][k]<1);
				}
			}
		}
		//color = Scalar(0,(experimentsN*39)%130+20,(experimentsN*29) % 130+20);
	}

	void show(Mat &image){
		int xstart = x - weights.size()/2;
		int ystart = y + 50;

		if(debug_show_weights){
			for(int i = 1; i < weights.size(); i++){
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][0]),Point(xstart + i,ystart - weights.size()*5*weights[i][0]),Scalar(255,0,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][1]),Point(xstart + i,ystart - weights.size()*5*weights[i][1]),Scalar(0,255,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][2]),Point(xstart + i,ystart - weights.size()*5*weights[i][2]),Scalar(0,0,255));
			}
			for(int i = 1; i < weights.size(); i++){
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][3]),Point(xstart + i,ystart - weights.size()*5*weights[i][3]),Scalar(255,255,0));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][4]),Point(xstart + i,ystart - weights.size()*5*weights[i][4]),Scalar(0,255,255));
				line(image, Point(xstart + i-1,ystart - weights.size()*5*weights[i-1][5]),Point(xstart + i,ystart - weights.size()*5*weights[i][5]),Scalar(255,0,255));
			}
		}

		if(debug_show_functuins){
			for(int i = 1; i < sumdata.size(); i++){
				line(image, Point(xstart + i-1,ystart - sumdata.size()*0.5*sumdata[i-1][0]),Point(xstart + i,ystart - sumdata.size()*0.5*sumdata[i][0]),Scalar(255,0,0));
				line(image, Point(xstart + i-1,ystart - sumdata.size()*0.5*sumdata[i-1][1]),Point(xstart + i,ystart - sumdata.size()*0.5*sumdata[i][1]),Scalar(0,255,0));
				line(image, Point(xstart + i-1,ystart - sumdata.size()*0.5*sumdata[i-1][2]),Point(xstart + i,ystart - sumdata.size()*0.5*sumdata[i][2]),Scalar(0,0,255));
			}
			for(int i = 1; i < sumdata.size(); i++){
				line(image, Point(xstart + i-1,ystart - sumdata.size()*5*sumdata[i-1][3]),Point(xstart + i,ystart - sumdata.size()*5*sumdata[i][3]),Scalar(255,255,0));
				line(image, Point(xstart + i-1,ystart - sumdata.size()*5*sumdata[i-1][4]),Point(xstart + i,ystart - sumdata.size()*5*sumdata[i][4]),Scalar(0,255,255));
				line(image, Point(xstart + i-1,ystart - sumdata.size()*5*sumdata[i-1][5]),Point(xstart + i,ystart - sumdata.size()*5*sumdata[i][5]),Scalar(255,0,255));
			}
		}

		cross(image, Point(x,y),color,thickness);
		if(underExperiment){
			if(experimentsN > divStarts*2 + 10){
				ellipse( image, Point(x,y), Size( 10, 10), 0, 0, 360, Scalar( 0, 200, 0 ), 2, 8, 0 );
			}else{
				ellipse( image, Point(x,y), Size( 10, 10), 0, 0, 360, Scalar( 0, 80, 150 ), 2, 8, 0 );
			}
		}
	}
	void highlite(Mat &image){
		color += Scalar(0,80,0);
		thickness+=3;
		show(image);
		thickness-=3;
		color -= Scalar(0,80,0);
	}
};

struct SUMS{
	vector<vector<double> > sum_hl, sum_hr, sum_vl, sum_vr;
};

/** Global variables */
SUMS Sums, TotalR, TotalL;
std::vector<vector<double> > SumH(const Mat &A);
std::vector<vector<double> > SumV(const Mat &A);
String eyepair_cascade_name = "haarcascade_mcs_eyepair_big.xml";
CascadeClassifier eyepair_cascade;
string window_name = "Capture - Eyepair detection";
RNG rng(12345);
int slider1,slider2,slider3,slider4,slider5;
int frameN;
Rect prevEyepairs;
bool prevExists = false;
Mat prevFrame;
const int MAXX = 1920, MAXY = 1080;
const int probDistannce = 15;
const int PROBS_MAXX = MAXX/probDistannce, PROBS_MAXY = MAXY/probDistannce;
double Probs[PROBS_MAXX][PROBS_MAXY];
Mat image(MAXY,MAXX,CV_8UC3);
vector<DataPoint> points;
int pointUnderExperiment;


double vectorDiff(vector<double> A, vector<double> B){
	int res = 0;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += abs(A[i] - B[i]);
	}
	return res;
}

double vectorDiffSq(vector<double> A, vector<double> B){
	int res = 0;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += pow(A[i] - B[i],2);
	}
}

double vectorDiffWeighted(vector<double> A, vector<double> B, vector<double> w){
	int res = 0;
	double w_sum;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += abs(A[i] - B[i])*w[i];
		w_sum += w[i];
	}
	return res/w_sum;
}

double vectorDiffSqWeighted(vector<double> A, vector<double> B, vector<double> w){
	int res = 0;
	double w_sum;
	int size = A.size();
	assert(size == B.size());
	for(int i = 0; i < size; i++){
		res += pow(A[i] - B[i],2)*w[i];
		w_sum += w[i];
	}
	return res/w_sum;
}

/** Result is <0 if A is closer to cur, >0 if B is closer to cur */
double vectorSmartCMP(vector<double> A, vector<double> B, vector<double> cur, vector<double> AWeights, vector<double> BWeights ){
	double choice = 0;
	double w_sum = 0;
	int size = A.size();
	assert(size == B.size() && size == cur.size());
	for(int i = 0; i < size; i++){
		for(int k = 0; k < 3; k++){
			double w = (B[i] - A[i])*AWeights[i]*BWeights[i]; //TODO: find correct formula
			double x = cur[i];
			double mid = (A[i] + B[i])/2.;
			choice += (x - mid)*w;
			w_sum += abs(w);
		}
	}
	choice /= w_sum;
	return choice;
}

double choiceFromChoiceHV(double choice_h, double choice_v, Point A, Point B){
	double w_h, w_v;
	w_h = abs(A.x - B.x) + 0.1 * abs(A.y - B.y);
	w_v = abs(A.y - B.y) + 0.1 * abs(A.x - B.x);// TODO: find good constants
	return choice_h * w_h + choice_v * w_v;
}

double dataPointCMP_h(DataPoint A, DataPoint B, vector<vector<double> > sums ){
	//TODO: add memorization
	double choice_h = 0;
	double w_h_sum = 0, w_v_sum = 0;
	for(int i = 0; i < sums.size(); i++){
		for(int k = 0; k < 3; k++){
			double w = (B.sumdata[i][k] - A.sumdata[i][k])*A.weights[i][k]*B.weights[i][k]; //TODO: find correct formula
			double x = sums[i][k];
			double mid = (A.sumdata[i][k] + B.sumdata[i][k])/2.;
			choice_h += (x - mid)*w;
			w_h_sum += abs(w);
		}
	}
	if(w_h_sum == 0)return 0;
	choice_h /= w_h_sum;
	return choice_h;
}

double dataPointCMP_v(DataPoint A, DataPoint B, vector<vector<double> > sums ){
	double  choice_v = 0;
	double  w_v_sum = 0;
	for(int i = 0; i < sums.size(); i++){
		for(int k = 3; k < 6; k++){
			double w = (B.sumdata[i][k] - A.sumdata[i][k])*A.weights[i][k]*B.weights[i][k];
			double x = sums[i][k];
			double mid = (A.sumdata[i][k] + B.sumdata[i][k])/2.;
			//double choicel = (xl - midl)*(wl>0?1:-1);
			choice_v += (x - mid)*w;
			w_v_sum += abs(w);
		}
	}
	if(w_v_sum == 0)return 0;
	choice_v /= w_v_sum;
	return choice_v;
}

double dataPointCMP(DataPoint A, DataPoint B, vector<vector<double> > sums ){
	double choice = 0, choice_h = 0, choice_v = 0;
	choice_h =dataPointCMP_h(A,B,sums);
	choice_v =dataPointCMP_v(A,B,sums);
	double res = choiceFromChoiceHV(choice_h, choice_v,Point(A.x,A.y),Point(B.x,B.y));
	return res;
}

double almostGauss(double x, double m, double D){
	return exp(-pow(x-m,2)/D);
}


double interpolationFunction_1(int Ax, int Ay, int Bx, int By, int curX, int curY, double choice_h, double choice_v){
	choice_h *= slider1/100;
	choice_v *= slider2/100;
	if(choice_h > 1){
		choice_h = 1;
	}
	if(choice_v > 1){
		choice_v = 1;
	}
	if(choice_h < -1){
		choice_h = -1;
	}
	if(choice_v < -1){
		choice_v = -1;
	}
	double res_h, res_v;
	double d_h = pow(Ax - Bx,2)*slider3/1000;
	double d_v = pow(Ay - By,2)*slider3/1000;
	res_h = abs(choice_h)/exp(pow(Ax + Bx - Ax*choice_h + Bx*choice_h - 2*curX,2)/(d_h));
	res_v = abs(choice_v)/exp(pow(Ay + By - Ay*choice_v + Bx*choice_v - 2*curY,2)/(d_v));
	return res_h + res_v;
}

double interpolationFunction(int Ax, int Ay, int Bx, int By, int curX, int curY, double choice_h, double choice_v){
	double res_A, res_B, res;
	double r_A = sqrt(pow(Ax - curX,2) + pow(Ay - curY,2));
	double distanceSq  = pow(Ax - Bx,2) + pow(Ay - By,2);
	double D = distanceSq*2;
	double choice = choiceFromChoiceHV(choice_h, choice_v, Point(Ax, Ay), Point(Bx, By));
	res_A = almostGauss(r_A,0,D) * choice;
	return res_A;
	//double d_h = pow(Ax - Bx,2)*slider3/1000;
	//double d_v = pow(Ay - By,2)*slider3/1000;
	//res_h = abs(choice_h)/exp(pow(Ax + Bx - Ax*choice_h + Bx*choice_h - 2*curX,2)/(d_h));
	//res_v = abs(choice_v)/exp(pow(Ay + By - Ay*choice_v + Bx*choice_v - 2*curY,2)/(d_v));
	//return res_h + res_v;
}

vector<vector<double> > concatSumsLR(vector<vector<double> > sumL, vector<vector<double> >sumR){
	vector<vector<double> > res = sumL;
	res.insert( res.end(), sumR.begin(), sumR.end() );
	return res;
}

vector<vector<double> > mergeHorizVert(vector<vector<double> > sum_h, vector<vector<double> >sum_v){ //3d + 3d -> 6d
	vector<vector<double> > res = sum_h;
	for(int i = 0; i < res.size(); i++){
		res[i].push_back(sum_v[i][0]);
		res[i].push_back(sum_v[i][1]);
		res[i].push_back(sum_v[i][2]);
	}
	return res;
}

vector<vector<double> > SUMS2vec(SUMS s){
	vector<vector<double> > res, sumst_h, sumst_v;
	sumst_h = concatSumsLR(s.sum_hl, s.sum_hr);
	sumst_v = concatSumsLR(s.sum_vl, s.sum_vr);
	res = mergeHorizVert(sumst_h,sumst_v);
	return res;
}


void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if  ( event == EVENT_LBUTTONDOWN ){
		DataPoint tmp(x,y,SUMS2vec(Sums));
		tmp.underExperiment = true;
		points.push_back(tmp);
		pointUnderExperiment = points.size() - 1;
	}
	else if  ( event == EVENT_RBUTTONDOWN ){//update dispersions
		//1. find closest cross to the cursor
		int min_distance = 0x7fffffff;
		int min_i;
		for(int i = 0; i < points.size(); i++){
			DataPoint &pt = points[i];
			int distanceSq = pow((x - pt.x),2) + pow((y - pt.y),2);
			if(min_distance > distanceSq){
				min_i = i;
				min_distance = distanceSq;
			}
		}
		//update
		points[min_i].underExperiment = true;
		pointUnderExperiment = min_i;
	}
	else if  ( event == EVENT_RBUTTONUP || event == EVENT_LBUTTONUP ){//finish experiments
		points[pointUnderExperiment].underExperiment = false;
		pointUnderExperiment = -1;
	}
	else if  ( event == EVENT_MBUTTONDOWN )
	{
		// cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
	}
	else if ( event == EVENT_MOUSEMOVE )
	{
		// cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;
	}
}

double getDirectionH(){ //deprecated
	double choice = 0;
	double w_sum = 0;
	for(int i = 0; i < Sums.sum_hl.size(); i++){
		double d = 0;
		double midl[3], midr[3], wl[3], wr[3];
		for(int k = 0; k < 3; k++){
			double wl = TotalR.sum_hl[i][k] - TotalL.sum_hl[i][k];
			double xl = Sums.sum_hl[i][k];
			double midl = (TotalL.sum_hl[i][k] + TotalR.sum_hl[i][k])/2.;
			//double choicel = (xl - midl)*(wl>0?1:-1);
			double choicel = (xl - midl)*wl;
			w_sum += abs(wl);

			double wr = TotalR.sum_hr[i][k] - TotalL.sum_hr[i][k];
			double xr = Sums.sum_hr[i][k];
			double midr = (TotalL.sum_hr[i][k] + TotalR.sum_hr[i][k])/2.;
			//double choicer = (xr - midr)*(wr>0?1:-1);
			double choicer = (xr - midr)*wr;
			choice += choicel + choicer;
			w_sum += abs(wr);
		}
	}
	//return choice/Sums.sum_hl.size();
	return choice/w_sum*3;
}

void cross(Mat &img, Point p, Scalar col, int size){
	line(img, p - Point(10,10), p+Point(10,10), col,size);
	line(img, p - Point(10,-10), p+Point(10,-10), col,size);
}

Point meanWeighted(){
	double res_x = 0, res_y = 0, sum_w, dres_x, dres_y;
	for(int i = 0; i < PROBS_MAXX; i++){
		for(int j = 0; j < PROBS_MAXY; j++){
			res_x += i * Probs[i][j];
			res_y += j * Probs[i][j];
			sum_w += Probs[i][j];
		}
	}
	res_x = res_x / sum_w * probDistannce;
	res_y = res_y / sum_w * probDistannce;
	dres_x = res_x - MAXX/2;
	dres_y = res_y - MAXY/2;
	return Point(MAXX/2 + dres_x*2, MAXY/2 + dres_y *2);
}

void showDir(){
	image.setTo(Scalar(255,255,255)); //white background leads to higher quality
	//image.setTo(Scalar(0,0,0)); //black background more comfortable for staring
	if(debug_show_interpolations && (pointUnderExperiment < 0 || !freezeUnderExperiment)){
		vector<double> gradient;
		double eps = 0.05; //TODO: find good value

	}
	for(int i = 0; i < points.size(); i++){
		points[i].show(image);
	}

	if(points.size() > 0 && (pointUnderExperiment < 0 || !freezeUnderExperiment)){
		int i_max;
		DataPoint v_max = points[0];
		for(int i = 0; i < points.size(); i++){
			if(dataPointCMP(v_max, points[i], SUMS2vec(Sums)) >0 ){
				v_max = points[i];
				i_max = i;
			}
		}
		v_max.highlite(image);
	}
	imshow("dir", image);
}

void updateProbs(){
	int size = points.size();
	vector<vector<double> >  choice_h, choice_v;
	for(int i = 0; i < size; i++){
		choice_h.resize(size);
		choice_v.resize(size);
		for(int j = 0; j < size; j++){
			choice_h[i].resize(size);
			choice_v[i].resize(size);
			choice_h[i][j] = dataPointCMP_h(points[i],points[j], SUMS2vec(Sums));
			choice_v[i][j] = dataPointCMP_v(points[i],points[j], SUMS2vec(Sums));
		}
	}
	for(int pr_x = 0; pr_x < PROBS_MAXX; pr_x++){
		//cout<<pr_x<<endl;
		for(int pr_y = 0; pr_y < PROBS_MAXY; pr_y++){
			Probs[pr_x][pr_y] = 0;
			for(int i = 0; i < size; i++){
				for(int j = 0; j < points.size(); j++){
					double choice_hd = choice_h[i][j];
					double choice_vd = choice_v[i][j];
					Probs[pr_x][pr_y] += interpolationFunction(points[i].x, points[i].y, points[j].x, points[j].y, pr_x*probDistannce, pr_y*probDistannce, choice_hd, choice_vd);
					if(Probs[pr_x][pr_y] != Probs[pr_x][pr_y]){//Debug output //ERROR!
						cout<<"NAN:"<<points[i].x<<" "<<points[i].y<<" "<<points[j].x<<" "<<points[j].y<<" "<<  pr_x*probDistannce<<" "<< pr_y*probDistannce<<" "<<choice_hd<<" "<<choice_vd<<endl;
					}
				}
			}
		}
	}
}


/** @function main */
int main( int argc, const char** argv ){
	VideoCapture cap(1); // open camera #1
	if(!cap.isOpened())  // check if we succeeded
		return -1;

	//create windows
	namedWindow("w1",1);
	namedWindow("dir",CV_GUI_NORMAL);
	setMouseCallback("dir", CallBackFunc, NULL);
	createTrackbar("l","w1",&slider1,1000);
	createTrackbar("2","w1",&slider2,1000);
	createTrackbar("3","w1",&slider3,1000);
	createTrackbar("4","w1",&slider4,1000);
	createTrackbar("5","w1",&slider5,1000);
	pointUnderExperiment = -1;

	//-- 1. Load the cascades
	if( !eyepair_cascade.load( eyepair_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

	//-- 2. Read the video stream
	Mat frame;
	frameN = 0;
	struct timeval frameStarts,curTime, calcFin, showFin;
	if(debug_time){
		gettimeofday(&frameStarts, NULL);
	}
	while( true ){

		//frame = cvQueryFrame( capture );
		cap>>frame;
		frameN++;
		if(debug_time){
			gettimeofday(&curTime, NULL);
			if(frameN%15 == 0){
				cout<<"FPS: "<<1000000.0/(curTime.tv_usec - frameStarts.tv_usec)<<endl;
			}
			frameStarts = curTime;
		}
		//imshow("Cam",frame);
		//-- 3. Apply the classifier to the frame

		if( !frame.empty() ){
			if(debug_show_frame){
				imshow("Camera", frame);
			}
			detectAndCalculate( frame , cap); //calculate global var Sums
			if(pointUnderExperiment != -1){// update points
				points[pointUnderExperiment].update_sums(SUMS2vec(Sums),frameN);
			}
		}
		else{
			printf(" --(!) No captured frame -- Break!");
			break;
		}

		if(debug_time){
			gettimeofday(&calcFin, NULL);
			if(frameN%15 == 0){
				cout<<"calc time: "<<(calcFin.tv_usec - frameStarts.tv_usec)<<endl;
			}
		}

		int c = waitKey(10);
		if( (char)c == 27 ) { break; }
		if(c == 'u' || calculate_interpolations && pointUnderExperiment < 0){
			updateProbs();
		}
		showDir();
		if(debug_time){
			gettimeofday(&showFin, NULL);
			if(frameN%15 == 0){
				cout<<"show time: "<<(showFin.tv_usec - calcFin.tv_usec)<<endl;
			}
		}
	}
	return 0;
}

//Rect getEyesPosFromCam(VideoCapture &cap){
//	Mat frame;
//	cap>>frame;
//	std::vector<Rect> eyepairs;
//	Mat frame_gray;
//	cvtColor( frame, frame_gray, CV_BGR2GRAY );
//	equalizeHist( frame_gray, frame_gray );
//	eyepair_cascade.detectMultiScale( frame_gray, eyepairs, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(15, 15) );
//	return eyepairs[0];
//}

/** @function detectAndCalculate */
void detectAndCalculate( Mat frame , VideoCapture &cap)
{
	//-- Detect eyes
	std::vector<Rect> eyepairs;
	Mat frame_gray;
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );
	eyepair_cascade.detectMultiScale( frame_gray, eyepairs, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

	if(eyepairs.size()){
		size_t eyepairit = 0;
		int cutTop = 80,cutBottom = 80; //cut eyebrows
		const int funSize = 500; //size of each eye's image. //TODO: find good size
		int rHeight = funSize/2+cutTop+cutBottom, rWidth = funSize; //Image must be squre, to allow dataPointCMP() work.

		if(!prevExists){
			prevExists = true;
			prevEyepairs = eyepairs[eyepairit];
			prevFrame = frame.clone();
		}

		// mixing frame with previous to reduce noise
		double frameMixAlpha = 0.5;
		Mat nextFrame = frame.clone();
		addWeighted( nextFrame, frameMixAlpha, prevFrame, 1-frameMixAlpha, 0.0, nextFrame);
		prevFrame = frame.clone();
		frame = nextFrame.clone();

		//stabilization of eye detection
		double mult =2;
		eyepairs[eyepairit].x = (eyepairs[eyepairit].x + mult*prevEyepairs.x)/(1.0 + mult);
		eyepairs[eyepairit].y = (eyepairs[eyepairit].y + mult*prevEyepairs.y)/(1.0 + mult);
		eyepairs[eyepairit].width = (eyepairs[eyepairit].width + mult*prevEyepairs.width)/(1.0 + mult);
		eyepairs[eyepairit].height = (eyepairs[eyepairit].height + mult*prevEyepairs.height)/(1.0 + mult);
		prevEyepairs = eyepairs[eyepairit];

		//Mat eyepairROI = frame_gray( eyepairs[eyepairit] );
		Mat eyepairROIc = frame(eyepairs[eyepairit]);
		Mat eyepairROIcr;
		Mat leftHalf, rightHalf;
		resize(eyepairROIc, eyepairROIcr,Size(rWidth, rHeight), 0, 0, CV_INTER_CUBIC);
		eyepairROIcr = eyepairROIcr(Rect(0, cutTop, rWidth, rHeight - cutTop - cutBottom));
		rHeight -= cutBottom + cutTop;
		leftHalf = eyepairROIcr(Rect(0,0,rWidth/2,rHeight));
		rightHalf = eyepairROIcr(Rect(rWidth/2,0,rWidth/2,rHeight));


		Sums.sum_hl = SumH(leftHalf); //pice of code, the function was called for
		Sums.sum_vl = SumV(leftHalf);
		Sums.sum_hr = SumH(rightHalf);
		Sums.sum_vr = SumV(rightHalf);

		if(debug_show_eyes){
			Mat leftRes = leftHalf.clone();
			Mat rightRes = rightHalf.clone();
			Mat leftRes_v = leftHalf.clone();
			Mat rightRes_v = rightHalf.clone();
			ShowFunctions_hc(leftHalf,leftRes);
			ShowFunctions_vc(rightHalf,leftRes_v);
			imshow( window_name+std::to_string(eyepairit), leftRes);
			imshow( window_name+std::to_string(eyepairit)+"v", leftRes_v);
			ShowFunctions_hc(rightHalf,rightRes);
			ShowFunctions_vc(rightHalf,rightRes_v);
			imshow( window_name+std::to_string(eyepairit)+"1", rightRes);
			imshow( window_name+std::to_string(eyepairit)+"1"+"v", rightRes_v);
		}

		// some experiments

		//cvtColor( eyepairROIcr, eyepairROIr_gs, CV_BGR2GRAY );
		//equalizeHist( eyepairROIr_gs, eyepairROIr_gs );
		//GaussianBlur(eyepairROIr_gs,eyepairROIcr_blur,Size(15,15),4,4);
		//eyepairROIr_gs_antilight = (eyepairROIr_gs*slider1 / eyepairROIcr_blur);
		//GaussianBlur(eyepairROIcr,eyepairROIcr_blur,Size(15,15),4,4);
		//eyepairROIcr_antilight = (eyepairROIcr*slider1 / eyepairROIcr_blur);

		//Mat edges = eyepairROIcr.clone();
		//GaussianBlur(edges, edges, Size(2*(slider2/100+1)+1,2*(slider2/100+1)+1), slider1/100., slider1/100.);
		//edges *= slider3/200.;
		//Canny(edges, edges, 0, 30, 3);
		//GaussianBlur(edges, edges, Size(2*(slider3/100+1)+1,2*(slider3/100+1)+1), slider4/100., slider4/100.);
		//imshow("edges", edges*slider5/100.);


		//Mat leftFlipped, rightFlipped;
		//flip(leftHalf,leftFlipped, 1);
		//flip(rightHalf,rightFlipped, 1);
		//imshow( window_name+std::to_string(eyepairit)+"2", leftFlipped);
		//imshow( window_name+std::to_string(eyepairit)+"3", rightFlipped);
		//ShowFunctions_hc(eyepairROIcr);
		//imshow( window_name+std::to_string(eyepairit), eyepairROIcr);
		//ShowFunctions_h_gs2(eyepairROIr_gs);
		//imshow( window_name+std::to_string(eyepairit)+"1", eyepairROIr_gs);
		//ShowFunctions_hc(eyepairROIcr_antilight);
		//imshow( window_name+std::to_string(eyepairit)+"2", eyepairROIcr_antilight);
		//ShowFunctions_h_gs(eyepairROIr_gs_antilight);
		//imshow( window_name+std::to_string(eyepairit)+"3", eyepairROIr_gs_antilight);
		//ShowFunctions_hc(eyepairROIcr_blur);
		//imshow( window_name+std::to_string(eyepairit)+"4", eyepairROIcr_blur);
	}
}

void ShowFunctions_h_gs(Mat &A){ //deprecated
	std::vector<double> hsumGS;
	hsumGS.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		hsumGS[i] = 1;
	}

	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			hsumGS[j]+= A.at<cv::Vec3b>(i,j)[0]/255.0+(slider2/500.);
		}
	}
	for(int j = 0; j < A.cols; j++){
		hsumGS[j] = (hsumGS[j] - slider3)*(slider4+1)/(slider5+1);
	}

	for(int i = 1; i < A.cols; i++){
		line(A, Point(i-1,hsumGS[i-1]), Point(i,hsumGS[i]),Scalar(255,0,0),1);
	}
}

void ShowFunctions_h_gs2(Mat &A){ //deprecated
	std::vector<double> hsumGS;
	hsumGS.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		hsumGS[i] = 1;
	}

	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			hsumGS[j]*= A.at<cv::Vec3b>(i,j)[0]/255.0+(slider2/500.);
		}
	}
	for(int j = 0; j < A.cols; j++){
		hsumGS[j] = (hsumGS[j] - slider3)*(slider4+1)/(slider5+1);
	}

	for(int i = 1; i < A.cols; i++){
		line(A, Point(i-1,hsumGS[i-1]), Point(i,hsumGS[i]),Scalar(255,0,0),1);
	}
}

std::vector<vector<double> > SumH(const Mat &A){
	std::vector<vector<double> > res;
	double E[3] = {0,0,0};
	res.resize(A.cols);
	for(int i = 0; i < A.cols; i++){
		res[i].resize(3);
		res[i][0] = 0;
		res[i][1] = 0;
		res[i][2] = 0;
	}
	for(int i = 0; i < A.rows; i++){
		for(int j = 0; j < A.cols; j++){
			for(int k = 0; k < 3; k++)
				res[j][k] += A.at<cv::Vec3b>(i,j)[k]/256.;
		}
	}
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++)
			E[k] += res[j][k]/A.cols;
	}
	double min[3] = {10e30,10e30,10e30}, max[3] = {-10e30,-10e30,-10e30};
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++){
			//res[j][k] -= E[k];
			if(min[k] > res[j][k])
				min[k] = res[j][k];
			if(max[k] < res[j][k])
				max[k] = res[j][k];
		}
	}
	double delta[3] = {max[0] - min[0], max[1] - min[1], max[2] - min[2]};
	double mid[3] = {(max[0] + min[0])/2, (max[1] + min[1])/2, (max[2] + min[2])/2};
	for(int j = 0; j < A.cols; j++){
		for(int k = 0; k < 3; k++){
			res[j][k] -= mid[k];
			res[j][k] /= delta[k];
		}
	}
	return res;
}

std::vector<vector<double> > SumV(const Mat &A){
	Mat B;
	transpose(A,B);
	return SumH(B);
}

void ShowFunctions_hc(const Mat &A, Mat &out){
	int mid = A.rows/2;
	std::vector<std::vector<double> > sum;
	sum = SumH(A);
	for(int i = 1; i < A.cols; i++){
		for(int k = 0; k < 3; k++)
			line(out, Point(i-1,-sum[i-1][k]*A.rows + mid), Point(i,-sum[i][k]*A.rows + mid),Scalar(255*(k==0),255*(k==1),255*(k==2)),1);
	}
}

void ShowFunctions_vc(const Mat &B, Mat &out){
	Mat A,C;
	transpose(B,A);
	transpose(out,C);
	int mid = A.rows/2;
	std::vector<std::vector<double> > sum;
	sum = SumH(A);
	for(int i = 1; i < A.cols; i++){
		for(int k = 0; k < 3; k++)
			line(C, Point(i-1,sum[i-1][k]*A.rows + mid), Point(i,sum[i][k]*A.rows + mid),Scalar(255*(k==0),255*(k==1),255*(k==2)),1);
	}
	transpose(C,out);

}
#endif
